import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import html2canvas from 'html2canvas';
import * as htmlToImage from 'html-to-image';
import { toPng, toJpeg, toBlob, toPixelData, toSvg } from 'html-to-image';
import { capture, OutputType } from 'html-screen-capture-js';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'etran-qr-code';
  scannerEnabled: boolean = true;
  transports: Transport[] = [];
  information: string = 'สแกน';
  params : any;

  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    this.route.queryParams.subscribe((params) => {
      this.params = params;
    });
  }

  public scanSuccessHandler(event: any) {
    // this.scannerEnabled = false;
    this.information = event;
    window.location.replace(this.params.redirect);
  }

  public enableScanner() {
    this.scannerEnabled = !this.scannerEnabled;
    this.information =
      'No se ha detectado información de ningún código. Acerque un código QR para escanear.';
  }
}

interface Transport {
  plates: string;
  slot: Slot;
}

interface Slot {
  name: string;
  description: string;
}
